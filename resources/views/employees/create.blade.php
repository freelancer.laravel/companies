@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Employees</div>

                <div class="card-body">
                   
                    <form method="post" action="{{route('employees.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                            <label for="exampleInputFirst_name" class="form-label">First Name</label>
                            <input name="first_name" type="text" class="form-control" value="{{old('first_name')}}" id="exampleInputFirst_name">
                            @error('first_name')
                                <div class="form-text">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputLast_name" class="form-label">Last Name</label>
                            <input name="last_name" type="text" class="form-control" value="{{old('last_name')}}" id="exampleInputLast_name">
                            @error('last_name')
                                <div class="form-text">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Email address</label>
                            <input name="email" type="email" class="form-control" value="{{old('email')}}" id="exampleInputEmail1" aria-describedby="emailHelp">
                            @error('email')
                                <div class="form-text">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputPhone" class="form-label">Phone</label>
                            <input name="phone" type="text" class="form-control" value="{{old('phone')}}" id="exampleInputPhone">
                            @error('phone')
                                <div class="form-text">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <select name="company_id" class="form-select form-select-lg mb-3" aria-label="Default select example">
                                <option value="">Select company</option>
                                @foreach($companies as $company)
                                    @if(old('company_id') == $company->id)
                                        <option selected value="{{$company->id}}">{{$company->name}}</option>
                                    @else
                                        <option value="{{$company->id}}">{{$company->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                            @error('company_id')
                                <div class="form-text">{{$message}}</div>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
