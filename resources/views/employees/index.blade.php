@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">Employees</div>

                <div class="card-body">
                    <a href="{{route('employees.create')}}" class="btn btn-success mt-3 mb-3" role="button" data-bs-toggle="button">Add</a>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">First Name</th>
                                <th scope="col">Last Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Phone</th> 
                                <th scope="col">Company</th>
                                <th scope="col">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($employees as $employee)
                            <tr>
                                <th scope="row">{{$employee->id}}</th>
                                <td>{{$employee->first_name}}</td>
                                <td>{{$employee->last_name}}</td>
                                <td>{{$employee->email}}</td>
                                <td>{{$employee->phone}}</td>
                                <td>{{$employee->company->name}}</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{route('employees.edit',['employee' => $employee->id])}}" class="btn btn-primary">Edit</a>
                                        <a onclick="event.preventDefault();
                                                     document.getElementById('delete-employee').submit();" class="btn btn-danger">Delete</a>
                                    </div>
                                    <form id="delete-employee" action="{{route('employees.destroy', ['employee' => $employee->id])}}" method="POST" class="d-none">
                                        @csrf
                                        {{ method_field('DELETE') }}
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="pagination mt-5">
                        {{ $employees->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
