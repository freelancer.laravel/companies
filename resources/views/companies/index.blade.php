@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">Companies</div>

                <div class="card-body">
                    <a href="{{route('companies.create')}}" class="btn btn-success mt-3 mb-3" role="button" data-bs-toggle="button">Add</a>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Website</th>
                                <th scope="col">Logo</th>
                                <th scope="col">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($companies as $company)
                            <tr>
                                <th scope="row">{{$company->id}}</th>
                                <td>{{$company->name}}</td>
                                <td>{{$company->email}}</td>
                                <td>{{$company->phone}}</td>
                                <td>{{$company->website}}</td>
                                <td>{{$company->logo}}</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{route('companies.edit',['company' => $company->id])}}" class="btn btn-primary">Edit</a>
                                        <a onclick="event.preventDefault();
                                                     document.getElementById('delete-company').submit();" class="btn btn-danger">Delete</a>
                                    </div>
                                    <form id="delete-company" action="{{route('companies.destroy', ['company' => $company->id])}}" method="POST" class="d-none">
                                        @csrf
                                        {{ method_field('DELETE') }}
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="pagination mt-5">
                        {{ $companies->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
