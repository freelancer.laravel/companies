@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Companies</div>

                <div class="card-body">
                   
                    <form method="post" action="{{route('companies.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                            <label for="exampleInputname" class="form-label">Name</label>
                            <input name="name" type="text" class="form-control" value="{{old('name')}}" id="exampleInputname">
                            @error('name')
                                <div class="form-text">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Email address</label>
                            <input name="email" type="email" class="form-control" value="{{old('email')}}" id="exampleInputEmail1" aria-describedby="emailHelp">
                            @error('email')
                                <div class="form-text">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputPhone" class="form-label">Phone</label>
                            <input name="phone" type="text" class="form-control" value="{{old('phone')}}" id="exampleInputPhone">
                            @error('phone')
                                <div class="form-text">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputWebsite" class="form-label">Website</label>
                            <input name="website" type="text" class="form-control" value="{{old('website')}}" id="exampleInputWebsite">
                            @error('website')
                                <div class="form-text">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputLogo" class="form-label">Logo</label>
                            <input name="logo" type="file" class="form-control" value="{{old('logo')}}" id="exampleInputLogo">
                            @error('logo')
                                <div class="form-text">{{$message}}</div>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
