@extends('layouts.app')

@push ('scripts')
<script type="text/javascript">
    $(document).on('click','.close_image', function(){
       $(this).closest('.box-images').find('.old_image').val("");
       $(this).closest('.box-images').find('.show_image').attr("src", "");
       $(this).closest('.box-images').find('.close_image').remove();
    });
</script>
@endpush
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Companies</div>

                <div class="card-body">
                   
                    <form method="post" action="{{route('companies.update', ['company' => $company->id])}}" enctype="multipart/form-data">
                        @csrf
                        {{ method_field('PUT') }}
                        <div class="mb-3">
                            <label for="exampleInputname" class="form-label">Name</label>
                            <input name="name" type="text" value="{{$company->name}}" class="form-control" id="exampleInputname">
                            @error('name')
                                <div class="form-text">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Email address</label>
                            <input name="email" type="email" value="{{$company->email}}" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                            @error('email')
                                <div class="form-text">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputPhone" class="form-label">Phone</label>
                            <input name="phone" type="text" value="{{$company->phone}}" class="form-control" id="exampleInputPhone">
                            @error('phone')
                                <div class="form-text">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputWebsite" class="form-label">Website</label>
                            <input name="website" type="text" value="{{$company->website}}" class="form-control" id="exampleInputWebsite">
                            @error('website')
                                <div class="form-text">{{$message}}</div>
                            @enderror
                        </div>
                        <div class="mb-3 box-images">
                            <label for="exampleInputLogo" class="form-label">Logo</label>
                            <div class="wrap_image">
                                <input class="old_image" type="hidden" name="old_logo" value="{{$company->logo}}">
                                <img class="show_image" src="{{url('storage/' . $company->logo)}}">
                                <img class="close_image" src="{{asset('img/close.jpeg')}}"/>
                            </div>
                            <input name="logo" type="file" value="" class="form-control" id="exampleInputLogo">
                            @error('logo')
                                <div class="form-text">{{$message}}</div>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
