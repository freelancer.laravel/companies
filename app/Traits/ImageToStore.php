<?php

namespace App\Traits;

use Image;

/**
 * Save image in storage and put path image to DB
 *
 * @param $request
 * @return string path image from DB else empty
 */
trait ImageToStore {
    
    public function saveImage($request) {
        
        $old_logo = $request->input('old_logo');
        
        $image = $request->file('logo');
        
        if ($image) {
            
            $input['imagename'] = time() . '.' . $image->extension();

            $destinationPath = public_path('/storage/companies');

            $img = Image::make($image->path());

            $img->fit(400)->save($destinationPath . '/' . $input['imagename']);

            return 'companies/' . $input['imagename'];
            
        } else {
            
            if ($old_logo) {
                
                return $old_logo;
                
            } else {
                
                return '';
                
            }
        }
    }
}
